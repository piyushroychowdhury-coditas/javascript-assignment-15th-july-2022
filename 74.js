const user1 = {
    firstName : "Piyush",
    age: 22,
    about: function(){
        console.log(this.firstName, this.age);
    }   
}

// we should never store the call function because it will not be able to use the this reference
//and will show undefined, to store it, we need to use the bind method.

// user1.about();
const myFunc = user1.about.bind(user1);
myFunc();