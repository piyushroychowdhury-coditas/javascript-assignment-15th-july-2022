//WHEN WE WANT OUR OBJECT TO RETURN UNDEFINED EVEN WHEN WE WANT TO ACCESS ANYTHING WHICH
//IS AVAILABLE TO SOME OTHER OBJECT

const obj1 = {
    key1: "value1",
    key2: "value2"
}

const obj2 = Object.create(obj1); // {}
// there is one more way to create empty object
obj2.key3 = "value3";
// obj2.key2 = "unique";
console.log(obj2);

console.log(obj2.__proto__); //proto is taking us to obj1 when we are trying to access something in obj2
//which is not available to it.