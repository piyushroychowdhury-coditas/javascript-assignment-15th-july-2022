//DO-WHILE LOOP IN JAVA

//Printing from 0 to 9
let num = 0;
do{
    console.log(num);
    num++;
}while(num<=9);

//THIS DO-WHILE LOOP WORKS ATLEAST ONCE, BECAUSE IN DO-WHILE LOOP
//FIRST DO WORKS ATLEAST ONCE AND THEN THE WHILE IS CHECKED