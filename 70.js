//OPTIONAL CHAINING


//WE USE OPTIONAL CHAINING SO THAT WE DON'T GET ERROR 
//RATHER WE GET UNDEFINED AS SOON AS WE TRY TO ACCESS SOMETHING WHICH IS NOT AVAILABLE
//?. is used for optional chaining
const user  = {
    firstName: "Piyush",
    // marks: {english: '90'}
}

console.log(user?.firstName);
console.log(user?.marks?.english);