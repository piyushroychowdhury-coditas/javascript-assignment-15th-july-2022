//TO REDUCE THE TASK OF CREATING OBJECTS MANUALLY EVERYTIME, WE CAN CREATE A FUNCTION
//WHICH CAN DO THE TASK FOR US

function createUser(firstName, lastName, email, age, address){
    const user = {};
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.age = age;
    user.address = address;
    user.about = function(){
        return `${this.firstName} is ${this.age} years old.`;
    };
    user.is18 =  function(){
        return this.age >= 18;
    }
    return user;
}

const user1 = createUser('Piyush', 'Roy', 'piyush@gmail.com', 22, "Pune, maharashtra");
console.log(user1);
const is18 = user1.is18();
const about = user1.about();
console.log(about);