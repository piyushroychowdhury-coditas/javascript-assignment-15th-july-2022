//OBJECT DESTRUCTURING

const brand = {
    brandName : "Samsung",
    product : "televison"
};

//WITHOUT DESTRUCTURING

//const brandName = brand.brandName;
//const product =brand.product;

//console.log(brandName +", " +product);


//USING DESTRUCTURING

const {brandName,product} = brand;

console.log(brandName+", "+product);

//ANOTHER WAY, IF WANT TO DECLARE SOME OTHER NAME TO THE VARIABLES

const {brandName:newBrandName, product:newProduct} = brand;

console.log(newBrandName+", "+newProduct);