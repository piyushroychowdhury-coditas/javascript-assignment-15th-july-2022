//REST PARAMETERS -->It joins all the available values assigned.

//EXAMPLE
//Adding all the numbers of an array

//let myArray = [1,2,3,4,5,6,7,8];



const addAll = (...myArray) => 
{
    let sum = 0;
    for(let array of myArray){
        sum+=array;
    }

    return sum;

}


const summation = addAll(2,2,2,2,2,2,2,2);
console.log(summation);