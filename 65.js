//ARRAY METHODS --> SPLICE METHOD
//TO DELETE OR INSERT ANYTHING FROM THE MIDDLE OF THE ARRAY WE USE SPLICE METHOD

//start  insert  delete

const array = [1,2,3,4,5,6,7,8,9];

//DELETING
array.splice(1,2); //STARTING FROM 1ST INDEX AND DELETING 2 ITEMS

//AFTER DELETING ANY ITEM FROM THE ARRAY, SPLICE RETURNS THE DELETED ITEM SO YOU CAN STORE IT IN A VARIBLE AND PRINT IT.
console.log(array);

//INSERTING
array.splice(1,0,2,3); //1 is starting index, 0 is nothing to delete, 2,3 are the things to be added.
console.log(array);