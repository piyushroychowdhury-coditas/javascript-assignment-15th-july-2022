//ARRAY METHODS --> REDUCE METHOD

const numbers = [1,2,3,4,5,10];


 const sum = numbers.reduce((accumulator, currentValue)=>{
     return accumulator + currentValue;
 });

 console.log(sum);


// accumulator , currentValue,  return 
// 1               2              3 
// 3               3              6
// 6               4              10
// 10              5              15
// 15              10             25