//HOW TO CLONE AN ARRAY

//METHOD 1: SLICE METHOD IS USED TO CLONE AN ARRAY

let array1 = ["a", "b"];
let array2 = array1.slice[0];

console.log(array1===array2); //THIS WILL RETURN FALSE BECAUSE A NEW ARRAY IS BEING CREATED
console.log(array2);



//METHOD 2: CONCAT CAN ALSO BE USED TO CLONE AN ARRAY.

let newArray1 = ["1" , "2"];
let newArray2 = [].concat(newArray1);

console.log(newArray2);




//METHOD 3: SPREAD OPERATOR CAN ALSO BE USED TO CLONE AN ARRAY.

let array = ["a" , "b"];
let newArray = [...array];

console.log(newArray);