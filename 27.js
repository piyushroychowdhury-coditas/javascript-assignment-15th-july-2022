//ARRRAYS PUSH-POP



let days = ["MONDAY", "TUESDAY", "WEDNESDAY"];

console.log(days);

//PUSH METHOD, PUSHES AN ELEMENT AT THE END OF THE ARRAY.
days.push("THURSDAY");

console.log(days);


//----------------------------------------------------------------------


//POPS THE LAST ELEMENT OUT OF THE ARRAY AND RETURNS THE POPPED ELEMENT.
days.pop();
console.log(days);


//----------------------------------------------------------------------


//ARRAYS SHIFT UNSHIFT

//UNSHIFT ADDS ELEMENTS AT THE BEGINNING OF THE ARRAY
days.unshift("SUNDAY")
console.log(days);

//----------------------------------------------------------------------

//SHIFT REMOVES ELEMENT FROM THE BEGINNING OF THE ARRAY AND RETURNS THE SAME.

days.shift();
console.log(days);

//push and pop is faster than shift and unshift

