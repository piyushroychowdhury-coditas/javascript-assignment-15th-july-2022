//ARRAY DESTRUCTURING

const myArray = ["item1" , "item2" , "item3"]

let [myVar1, myVar2] = myArray;

console.log(myVar1);
console.log(myVar2);

//Instead of using loops for getting array values explicitly, we can use Array destructuring