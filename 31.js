//USING CONST FOR CREATING ARRAY

const fruits = ["mangoes" , "bananas"];
console.log(fruits);


fruits.push("apples");
console.log(fruits);

//WE ARE ABLE TO PUSH IN CONSTANT ARRAYS AS THE WE ARE NOT CHANGING THE ADDRESS OF THE ARRAY
//BUT JUST PUSHING IN THE SAME ARRAY ADDRESS.