//FUNCTIONS IN JAVASCRIPT

//FUNCTION IS USED TO REDUCE THE LINES OF CODE.

//FUNCTION DECLARATIONS -->
function singHappyBirthday() {
    console.log("Happy Birthday to you!!!");
}

singHappyBirthday()




//FUNCTION WITH PARAMETERS TO ADD TWO NUMBERS 

function addTwoNumbers(num1, num2) {
    return num1 + num2;
}

const sumOfTwoNumbers = addTwoNumbers(5, 9);

console.log(sumOfTwoNumbers);

//FUNCTION TO CHECK IF A NUMBER IS EVEN OR NOT

function isEven(number) {
    return number % 2 === 0;
}

console.log(isEven(5));


//FUNCTION TO RETURN FIRST CHARACTER OF A STRING

function firstCharacter(myString) {
    console.log(myString[0]);
}

//Calling the function
firstCharacter("Hello");



//FINDING TARGET INDEX FROM AN ARRAY USING FUNCTION 

function findTarget(array, target) {
    for (let i = 0; i < array.length; i++) {
        if (array[i] === target) {
            return i;
        }
    }
            return -1;
}

//calling the function

const targetValue = findTarget([7,8,9,1,2],2);

console.log(targetValue);