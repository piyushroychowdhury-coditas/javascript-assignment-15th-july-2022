//METHODS - FUNCTION ISNIDE OBJECTS ARE CALLED AS METHODS

//EXAMPLE

const carBrand = {
    name: "Toyota",
    model: "fortuner",

    //function declarations can only be turned into methods?
    myCar: function(){
        console.log(`brand of the car is ${this.name} and model is ${this.model}`); //this, is the object inside my function
    }
}

carBrand.myCar();