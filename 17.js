//NESTED IF-ELSE CONDITION

let winningNumber = 21;

let userGuess = +prompt("Guess a number");

if(userGuess === winningNumber){
    console.log("Your guess is right");
}

else{
    if(userGuess < winningNumber){
        console.log("too low!");
    }
    else{
        console.log("too high");
    }
}