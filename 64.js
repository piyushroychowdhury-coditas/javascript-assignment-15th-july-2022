//ARRAY METHODS --> FILL METHOD
//CHANGES THE ORIGINAL ARRAY

//WE CAN HAVE AN ARRAY OF DESIRED SIZE AND SAME ELEMENTS USING FILL METHOD
//EXAMPLE -->
const myArray = new Array(10).fill(-1);
console.log(myArray);




//fill

// value   starting index   ending

const newArray = [2,9,5,7,1,3,4];

const result = newArray.fill(0,1,4);
console.log(result);