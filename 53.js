//PARAMETER DESTRUCTURING
const person = {
    firstName: "Piyush",
    gender: "male",
    age: 22
}


//WE ARE USING OBJECT DESTRUCTURING INSIDE PARAMETERS SO THAT WE CAN ACCESS THE OBJECTS
//WITHOUT USING DOT NOTATION
function printDetails({firstName, gender, age}){
    console.log(firstName);
    console.log(gender);
    console.log(age);
}

printDetails(person);