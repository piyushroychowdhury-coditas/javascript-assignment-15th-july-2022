//NESTED DESTRUCTURING

const marks = [
    {English:80,Maths:90,Science:78},
    {English:60,Maths:70,Science:80},
    {English:91,Maths:84,Science:89}
]

//marks1, marks2, marks3 are destructuring the above array of objects into single objects
const [marks1,marks2,marks3]=marks;

console.log(marks1);
console.log(marks2);
console.log(marks3);


//IF WE WANT TO DESTRUCTURE THE OBJECTS INSIDE THE marks ARRAY-->

const[{English}, ,{Science}]=marks;

console.log(English);
console.log(Science);