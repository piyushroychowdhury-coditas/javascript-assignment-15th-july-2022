//ARRAY METHODS --> EVERY METHOD

//AS THE NAME SUGGESTS IT COMPARES ALL THE ELEMENTS OF THE ARRAY TOGETHER.

const array = [12,16,14,18,20];

const isEven = (number) =>{
    return number%2 === 0;
}

const result = array.every(isEven); //EVERY RETURNS EITHER TRUE/FALSE

console.log(result);