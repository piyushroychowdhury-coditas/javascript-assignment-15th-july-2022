//COMPUTED PROPERTIES

const key1 = "objKey1";
const key2 = "objKey2";

const value1 = "myValue1";
const value2 = "myValue2";

//On creating an object if we want to compute the values of key1, key2
//and value1 and value2, we need to use [] brackets.

const myObject = {
    [key1] : value1,
    [key2] : value2
};

console.log(myObject);