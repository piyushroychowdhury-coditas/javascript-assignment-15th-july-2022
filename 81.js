//PROTOTYPES IN JAVASCRIPT
//PROTOTYPES ARE PRESENT ONLY IN FUNCTIONS
//IT IS A BUILT IN OBJECT
//WE CAN TREAT IT AS EMPTY OBJECT

function myFunc(){
    console.log("Inside my function");
}

if(myFunc.prototype){
    console.log("prototype is present");
}

else{
    console.log("prototype is not present");
}


//WE CAN ADD AS MANY KEY VALUE PAIRS TO IT, AS WE WANT
//EXAMPLE

myFunc.prototype.name = "Piyush";
myFunc.prototype.age = 22;

console.log(myFunc.prototype);
