//MAPS OBJECT
//STORES KEY - VALUE PAIR

//IN OBJECTS KEYS ARE MOSTLY STRINGS BUT IN MAP, KEYS CAN BE OF ANYTHING OTHER THAN JUST STRING

const myMap = new Map();

myMap.set("name", "Piyush")
myMap.set("age",22);

console.log(myMap);

console.log(myMap.get("name")); //GET METHOD IS USED IN MAPS TO RETEIVE DATA INSIDE THE MAP

console.log(myMap.keys());

myMap.set(1,"Mathematics");

for(let key of myMap.keys()){
    console.log(key,typeof(key));
}