//NEW KEYWORD --> CREATES AN EMPTY OBJECT

//EXAMPLE
function myFunc(name, address){
    this.name = name;
    this.address = address;
}

myFunc.prototype.details = function(){
    console.log(`name is ${this.name} and address is ${this.address}`);
}

const result = new myFunc("Piyush", "Pune");
console.log(result);

result.details();


//OPTIMIZING THE 82ND FILE'S CODE

function CreateUser(firstName, lastName, email, age, address){
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.age = age;
    this.address = address;
}
CreateUser.prototype.about = function(){
    return `${this.firstName} is ${this.age} years old.`;
};
CreateUser.prototype.is18 = function (){
    return this.age >= 18; 
}
CreateUser.prototype.sing = function (){
    return "la la la la ";
}


const user1 =  new CreateUser('Piyush', 'Roy', 'piyush@gmail.com', 22, "Pune - Maharashtra");
const user2 = new CreateUser('Rohan', 'Aggarwal', 'rohan@gmail.com', 25, "Pune - Maharashtra");
const user3 = new CreateUser('Ankit', 'Singh', 'ankit@gmail.com', 27, "Pune - Maharashtra");
console.log(user1);
console.log(user1.is18());