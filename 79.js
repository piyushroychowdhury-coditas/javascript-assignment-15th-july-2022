const userMethods = {
    about : function(){
        return `${this.firstName} is ${this.age} years old.`;
    },
    is18 : function(){
        return this.age >= 18;
    },
    sing: function(){
        return 'toon na na na la la ';
    }
}
function createUser(firstName, lastName, email, age, address){
    const user = Object.create(userMethods);// {}
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.age = age;
    user.address = address;
    return user;
}

const user1 = createUser('Piyush', 'Roy', 'piyush@gmail.com', 22, "Pune - Maharashtra");
const user2 = createUser('Rohan', 'Aggarwal', 'rohan@gmail.com', 25, "Pune - Maharashtra");
const user3 = createUser('Ankit', 'Singh', 'ankit@gmail.com', 27, "Pune - Maharashtra");
console.log(user1);
console.log(user1.about());
// console.log(user3.sing());