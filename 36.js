//OBJECTS IN JAVA SCRIPT

//creating objects

const student = {name: "Piyush Roy Chowdhury", Age: 22 , Class: "IX"};


//ACCESSING OBJECTS
console.log(student.name);
console.log(student.Age);
console.log(student.Class);

//BRACKET NOTATION TO ACCESS OBJECTS

console.log(student["name"]);
console.log(student["Age"]);
console.log(student["Class"]);

//ADDING MORE KEY VALUE PAIRS TO OBJECTS

student.gender = "male";
console.log(student);