//ARRAY METHODS --> FILTER METHOD
//TAKES CALLBACK FUNCTION AS PARAMETER
//RETURNS BOOLEAN VALUE

//EXAMPLE --> CHECKING FOR EVEN NUMBERS

const myArray = [1,2,3,4,5,6,7,8,9];

const isEven = (number) => {
    return number%2===0;
}

const resultArray = myArray.filter(isEven); //FILTER ALWAYS RETURN TRUE OR FALSE

console.log(resultArray);