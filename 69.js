// CLONE USING OBJECT.ASSIGN 
 

const obj = {
    key1: "value1",
    key2: "value2"
}

 //IN JAVASCRIPT OBJECTS CAN BE CLONED USING OBJECT.ASSIGN OR SPREAD OPERATOR
 const obj2 = Object.assign({'key69': "value69"}, obj);
obj.key3 = "value3";
 console.log(obj);
 console.log(obj2);