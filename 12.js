//Booleans and Comparision operators

//Booleans are gonna return either true or false
let num1 = 34;
let num2 = 98;

console.log(num1>num2); //this will return false

console.log(num2>=num1); //this will return true


// == vs ===

let newNum1 = 90;
let newNum2 = "90";

console.log(newNum1 == newNum2); //Lossy comparision will return "true"
console.log(newNum1 === newNum2); //Strict comparision will return "false"
