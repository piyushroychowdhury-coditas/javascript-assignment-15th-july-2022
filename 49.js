//LEXICAL SCOPE --> JAVASCRIPT SEARCHES FOR A NAMED VARIABLE IN THE LEXICAL SCOPE
//IF IT IS NOT AVAILABLE IN ITS LOCAL SCOPE.
const myVar = "value1";

function myApp(){
    function myFunc(){
        // const myVar = "value59";
        const myFunc2 = () =>{
            console.log("inside myFunc", myVar);
        }
        myFunc2();
    }
    console.log(myVar);
    myFunc();
}

myApp();