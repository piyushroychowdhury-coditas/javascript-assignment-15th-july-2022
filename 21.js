//SUM OF 10 NATURAL NUMBERS USING WHILE LOOP

let number = 1;
let sum = 0;

while(number <= 10){
    sum=sum+number;
    number++;
}

console.log(`the sum of 10 natural number are: ${sum}`);