//typeof operator --> tells us the types of the datatype used.

//types of primitive datatypes -->


//-------------------------------------------------


//1. Number -->
let num = 24;

//typeof num will print "number"
console.log(typeof num);


//-------------------------------------------------


//2. String -->
let firstName = "Piyush";

//typeof num will print "string"
console.log(typeof firstName);


//-------------------------------------------------


//CONVERTING NUMBER TO STRING-->
let age = 22;
console.log(typeof age);
age= age+"";
console.log(typeof age);

//-------------------------------------------------


//CONVERTING STRING TO NUMBER-->
let marks = "98";
console.log(typeof marks);

marks = +"98";
console.log(typeof marks);


//ANOTHER WAY OF CHANGING "NUMNER TO STRING" AND "STRING TO NUMBER" IS TYPECASTING
let newAge=32;
console.log(typeof newAge);


//CHANGING NUMBER TO STRING
newAge=String(newAge);
console.log(typeof(newAge));

//CHANGING STRING TO NUMBER

let newNum = "65";
newNum=Number(newNum);
console.log(typeof(newNum));