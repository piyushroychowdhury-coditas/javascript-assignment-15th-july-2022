//SETS
//they are also iterables
//they can store data
//only unique elements are allowed duplicates are ignored
//No order is maintained
//there is no indexed based access.

const mySet = new Set([9,8,7,6,5,4,3,2,1]);

console.log(mySet);

//WE CAN ALSO DECLARE AN EMPTY SET AND START STORING DIFFERENT VALUES OF DIFFERENT DATATYPES IN THE SET,
//USING ADD METHOD

const newSet = new Set();

newSet.add(1);
console.log(newSet);

//TO CHECK WHETHER A VALUE IS PRESENT IN A SET OR NOT, WE USE HAS METHOD

console.log(newSet.has(3));

//AS SETS ARE ITERABLES SO WE CAN USE FOR - OF LOOP WITH IT
