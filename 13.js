//Truthy and Falsy values
//false
//""
//undefined
//null
//0

let name="piyush";

if(name){
    console.log("NAME IS TRUTHY");
}

else{
    console.log("NAME IS FALSY");
}