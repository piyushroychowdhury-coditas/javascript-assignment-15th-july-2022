//FUNCTION RETURNING FUNCTION

const outerFunction = () => {
    const innerFunction = () => {
        console.log("inner function");
    }

    return innerFunction();
}

outerFunction();


//CALLBACK FUNCTIONS AND FUNCTION RETURNING A FUNCTION ARE CALLED AS HIGH ORDER FUNCTIONS