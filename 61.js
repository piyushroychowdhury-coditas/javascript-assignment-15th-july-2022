//ARRAY METHODS --> FIND METHOD

//used to fetch/find elements from the array
//accepts a callback back function.

const array = ["Piyush", "Ravi" , "Sneha" , "Aditi"];

const nameLengthGreaterThan4 = (string) => {
    return string.length>4;
}

const result = array.find(nameLengthGreaterThan4);
console.log(result); //Piyush will be printed --> because it checks only the first occurance of the meeting condition