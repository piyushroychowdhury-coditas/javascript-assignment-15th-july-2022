//  and or operator

// and operator

let marks = 95;
let subject = "maths";

//and operator checks for both the conditions and returns true if both the condition meets.
if(marks>60 && subject==="maths"){
    console.log("You can take maths as specialization");
}

else{
    console.log("You cannot take maths as specialization")
}


//------------------------------------------------------------------


// or operator

let name = "Rahul";
let age = 34;


//or opeartor checks for true in any one of the conditions and returns true if one of them meets.
if(name[0]==="P" || age>=18){
    console.log("You are allowed for the party");
}
else{
    console.log("You are not allowed for the party");
}

