// IF, ELSE-IF, ELSE-IF, ELSE-IF, ELSE CONDITION

let marks = 97;

if( marks < 33 ){
    console.log("You are not eligible for ant stream");
}

else if (marks < 50){
    console.log("You are eligible for humanities stream");
}

else if (marks < 80){
    console.log("You are eligible for commerce stream");
}

else{
    console.log("You are eligible for science stream");

}

console.log("execution completed");