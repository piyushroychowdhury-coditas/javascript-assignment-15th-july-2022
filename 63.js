//ARRAY METHODS --> SOME METHOD

//IT CHECKS FOR ATLEAST ONE CONDITION THAT MATCHES THE REQUIRED CONDITION AND RETURNS TRUE

//EXAMPLE

const array = [13 , 17 , 18 , 9 , 5];

const result = array.some((number) => {
    return number%2===0;
})

console.log(result);
