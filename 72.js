//this --> console.log(this) will provide us with a window object.
//console.log(window) will also work the same

"use strict";
function myFunc() {

    console.log(this);
}
myFunc();