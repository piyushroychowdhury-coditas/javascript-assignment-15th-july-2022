//PRIMITIVE VS REFERENCE DATA TYPES


//PRIMITIVE DATA TYPES
let num1= 10;
let num2 = num1;

console.log(num1);
console.log(num2);

num1++;

console.log(num1);
console.log(num2);


//REFERENCE DATA TYPES
let array1 = ["item1","item2"];
let array2 = array1;

console.log(array1);
console.log(array2);

array1.push("item3");

console.log(array1);
console.log(array2);

//IN PRIMITIVES WE HAVE MEMEORY ALLOCATED SEPARATELY IN STACK WHERE AS 
// IN REFERENCE THE MEMORY ALLOCATED IS ONLY ONCE AND IS STORED IN THE HEAP.