// ARROW FUNCTIONS --> "this" will not work with arrow function because arrow function takes "this"
//from the surrounding

const user1 = {
    firstName : "Piyush",
    age: 22,
    about: () => {
        console.log(this.firstName, this.age);
    }   
}

user1.about(user1);