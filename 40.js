//SPREAD OPERATOR --> if we want to spread the elements of an array or objects we can use
//spread operators

//EXAMPLE IN ARRAY -- >

const array1 = [2,3,4];
const array2 = [9,0,8];

const newArray = [...array1,...array2];

console.log(newArray);


//SIMILARLY LIKE ARRAYS WE CAN USE SPREAD OPERATORS IN OBJECTS TOO

const myObject1 = {
    key1 : "Piyush",
    key2 : "Pranav"
};

const myObject2 = {
    key3 : "Prakash",
    key4 : "Aditya"
};

const newObject = {
    ...myObject1,...myObject2
};

console.log(newObject);


//SPREADING ELEMENTS OF A STRING

const stringObject = {..."ABCDEF"};

console.log(stringObject);


