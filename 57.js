//MAP METHOD - ARRAY METHODS
//IT CREATES AN ARRAY OF THE RESULT

//EXAMPLE

const array = [1,2,3,4,5,6,7];

const myFunction = (number) => {
    return number*2; 
}

const result = array.map(myFunction); //result will be an array of numbers, multipled by 2

console.log(result);