//ARRAY METHODS -->
//FOR - EACH METHOD
//takes function as input
//somewhat does the work of the loop

//EXAMPLE to print the index and numbers of the array.

const myArray = [3,4,5,6,7,8,9];

const myFunc = (number , index) => {
    console.log(`index is ${index} and the number is ${number}`);
}

//for(let i = 0 ; i < myArray.length ; i++){
  //  myFunc(myArray[i],i);
//}

myArray.forEach(myFunc);

