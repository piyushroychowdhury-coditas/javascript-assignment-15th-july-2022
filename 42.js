//OBJECTS INSIDE AN ARRAY

const marks = [
    {English:80,Maths:90,Science:78},
    {English:60,Maths:70,Science:80},
    {English:91,Maths:84,Science:89}
]

for(let mark of marks){
    console.log(mark);
}