//IN THE PREVIOUS FILE, WE UNDERSTOOD HOW TO CREATE MULTIPLE OBJECTS EFFICIENTLY
//BUT TWO METHODS WERE ALWAYS REPEATING ITSELF WHEN WE KNOW THEY CAN BE JUST DEFINED ONCE
//AND USED MULTIPLE TIMES.

const userMethods = {
    about : function(){
        return `${this.firstName} is ${this.age} years old.`;
    },
    is18 : function(){
        return this.age >= 18;
    }
}
function createUser(firstName, lastName, email, age, address){
    const user = {};
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.age = age;
    user.address = address;
    user.about = userMethods.about;
    user.is18 = userMethods.is18;
    return user;
}

const user1 = createUser('Piyush', 'Roy', 'piyush@gmail.com', 22, "Pune - Maharashtra");
const user2 = createUser('Rohan', 'Aggarwal', 'rohan@gmail.com', 25, "Pune - Maharashtra");
const user3 = createUser('Ankit', 'Singh', 'ankit@gmail.com', 27, "Pune - Maharashtra");
console.log(user1.about());
console.log(user3.about());