//ARROW FUNCTIONS
const favouriteMovie = () =>{
    console.log("SHAWSHANK REDEMPTION");
}

favouriteMovie();


//EXAMPLE 2 -->
const addTwoNumbers = (num1, num2) => {
    return num1 + num2;
}

const sumOfTwoNumbers = addTwoNumbers(5, 9);

console.log(sumOfTwoNumbers);