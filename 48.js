//FUNCTIONS INSIDE FUNCTIONS

const outerFunction = () =>{

    const innerFunction1 = () => {
        console.log("inner function 1");
    }

    const innerFunction2 = () => {
        console.log("inner function 2");
    }

    console.log("outer function");
    innerFunction1();
    innerFunction2();
}

outerFunction();