//CLASSES IN JAVASCRIPT
//THE THINGS WE DID USING FUNCTIONS/METHODS CAN BE ALSO DONE WITH THE HELP OF CLASSES AND 
//CONSTRUCTORS

class CreateUser{
    constructor(firstName, lastName, email, age, address){
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.age = age;
        this.address = address;
    }

    about(){
        return `${this.firstName} is ${this.age} years old.`;
    }
    is18(){
        return this.age >= 18;
    }
    sing(){
        return "la la la la ";
    }

}


const user1 =  new CreateUser('Piyush', 'Roy', 'piyush@gmail.com', 22, "Pune - Maharashtra");
const user2 = new CreateUser('Rohan', 'Aggarwal', 'rohan@gmail.com', 25, "Pune - Maharashtra");
const user3 = new CreateUser('Ankit', 'Singh', 'ankit@gmail.com', 27, "Pune - Maharashtra");
 console.log(Object.getPrototypeOf(user1));