//block scope vs function scope

//let and const -->block scope--> That means they are only valid within the block they are declared


{
    let subject = "Mathematics";
}

console.log(subject); //This will throw an error.



//var is function scope
//--> That means we can use var when we want to use a variable outside it's block

{
    var name = "Piyush";
}

console.log(name);