//undefined

let name; //name is declared and not defined so it will be assigned with UNDEFINED.
var age;
//LET and VAR can be declared and not be defined but CONST has to always be defined. It cannot hold UNDEFINED.
 

//null
let newName = null;
//There's a bug in JavaScript where the typeof of null returns object which is not true.


//bigint
//If we want to store some value which is bigger than the safe integer value then we use BIGINT.

let myNumber =123;
console.log(Number.MAX_SAFE_INTEGER);

let newNumber = BigInt(1234567729288727123123132);
console.log(newNumber);

//BigInt cannot be used with other datatypes, so it has to be used explicitly. 