//DIFFERENCE BETWEEN DOT AND BRACKET NOTATION

const key = "email";

const person = {
    "name": 'Piyush', 
    "age": 22,
    "person hobbies": ["cricket" , "guitar" ,"singing"]
};

//WE CANNOT ACCESS PERSON HOBBIES USING DOT NOATION SO WE HAVE TO USE BRACKET NOTATION FOR IT.

console.log(person["person hobbies"]);

//IF WE WANT TO ADD A KEY OF OUR DESIRED CHOICE, WE CAN DO THAT USING BRACKET NOTATION
person[key]= "piyush.chowdhury@coditas.com";

console.log(person);