//using let keyword to declare variables

let firstName = "Piyush";
console.log(firstName);

//let can be used only once for declaring the same variable
//example

// let carBrand = "Audi";
// let carBrand = "BMW";
//The above lines of code will throw error.

var carBrand = "Audi";
var carBrand = "BMW";
console.log(carBrand);

//the above lines of code works the fine when var is used.