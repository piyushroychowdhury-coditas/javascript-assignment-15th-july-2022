 //IN OBJECTS, THE METHODS CAN BE DECLARED AND DEFINED DIRECTLY WITHOUT SPECIFYING THE KEY AND VALUE SEPARATELY
 
 const user1 = {
     firstName : "Pranav",
     age: 22,
     about: function(){
         console.log(this.firstName, this.age);
     }   
 }

 const user2 = {
     firstName : "Rahul",
     age: 23,
     about(){
         console.log(this.firstName, this.age);
     }   
 }



user2.about();