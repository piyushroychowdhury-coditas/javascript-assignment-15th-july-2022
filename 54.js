//callback functions
//functions that can be passed as a parameter are called as callback functions

//EXAMPLE
 const callBackFunction = () => {
    console.log ("INSIDE CALLBACK FUNCTION");
 }

 
 const outerFunction = (callback) => {
    callback();
 }

 outerFunction(callBackFunction);