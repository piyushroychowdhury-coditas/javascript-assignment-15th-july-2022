//WHILE - LOOP

//WHEN WE DON'T WANT TO REPEATEDLY CODE TO GET DESIRED OUTPUT, WE USE LOOPS.

let num = 0;
 
//PRINTING NUMBERS FROM 0 - 9 USING A WHILE LOOP.
while(num<=9){
    console.log(num);
    num++;
}

console.log(`the current value of num is ${num}`);


//----------------------------------------------------------

