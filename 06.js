//String indexing

let firstName = "Piyush";

// p  i  y  u  s  h
// 0  1  2  3  4  5

//Printing a specific character from specific index in a String.

console.log(firstName[4]);

//finding the length of the string (including the spaces in the string)
console.log(firstName.length);

//Printing the last character of the string
console.log(firstName[firstName.length-1]);