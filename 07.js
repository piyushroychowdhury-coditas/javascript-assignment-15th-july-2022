//Methods in String

//trim() --> Removes all the spaces from the beginning and the end of the string
let firstName = "    Piyush     ";

//Prints the length of the string including the spaces
console.log(firstName.length);

let modifiedFirstName = firstName.trim();

//Prints the length of the string without the spaces
console.log(modifiedFirstName.length);

//-------------------------------------------------------------------------------

//toUpperCase() --> Changes the string into all UPPERCASE
let carName = "Toyota";

//Prints the carName in all UPPERCASE.
console.log(carName.toUpperCase());


//-------------------------------------------------------------------------------

//toLowerCase()--> Changes the string into all lowercase
let cityName = "Pune";

//Prints the carName in all lowercase.
console.log(cityName.toLowerCase()); 

//-------------------------------------------------------------------------------

//slice -->slices the string from starting index to ending index
let bikeName = "Kawasaki";

//Prints the bikeName till Kawas
console.log(bikeName.slice(0,4));