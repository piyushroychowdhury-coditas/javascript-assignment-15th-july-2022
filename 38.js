//ITERATING OVER OBJECTS

const person = {
    "name": 'Piyush', 
    "age": 22,
    "person hobbies": ["cricket" , "guitar" ,"singing"]
};


//FOR-IN LOOP

for(let key in person){
    console.log(key+": "+person[key]);
}

//OBJECT.KEYS

console.log(Object.keys(person));