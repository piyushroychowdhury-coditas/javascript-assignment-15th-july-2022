//Rules for naming variables

//Should always start with a character( _ or $ )/alphabet 
var value1 = 2;
console.log(value1 ** 2);

//spaces cannot be used and $ and _ is only accepted in special characters
 var first$Name='Raj';
 var _lastName="Chopra";

 console.log(first$Name + _lastName);