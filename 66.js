//ITERABLES AND ARRAY LIKE OBJECTS

//ITERABLES --> ALL THE DATATYPES TO WHICH WE CAN APPLY FOR - OF LOOP ARE CALLED ITERABLES

//EXAMPLE IS STRING

const myString = "PIYUSH";

for(let char of myString){
    console.log(char);
}

//ARRAYS ARE ITERABLES

const myArray = [1,2,3,4,5,6,7];

for(let items of myArray){
    console.log(items);
}


//************OBJECTS ARE NOT ITERABLE**********


//ANY DATATYPE THAT HAS LENGHTH PROPERTY ARE CALLED ARRAY LIKE OBJECTS
//FOR EXAMPLE STRING
console.log("---------------------------------------------------");
const name = "SAHIL";
console.log(name.length);
console.log(name[3]);