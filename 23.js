//FOR LOOP TO ADD FIRST 10 NATURAL NUMBERS

let sum =0;

for(let num = 1 ; num <= 10 ; num++){
    sum = sum + num;
}

console.log(`The sum of 10 natural numbers are: ${sum}`);