//BREAK AND CONTINUE STATEMENTS IN JS

//BREAK IN LOOPS
let num;
for(num = 0; num < 10; num++){
    if(num === 5){
        break;
    }
}

console.log(num);



console.log("------------------------------------------------------");


//CONTINUE IN LOOPS
let newNum;
for(newNum = 0; newNum < 10; newNum++){
    if(newNum === 6){
        continue;
    }
    console.log(newNum);
}
